package com.pe.listado;

import android.content.Context;
import android.media.Image;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;


/**
 * Created by Riccardomp on 01/11/18.
 */

public class ApodoAdapter extends RecyclerView.Adapter<ApodoAdapter.ViewHolder> {

    List<Persona> lstApodos;
    Context context;

    public ApodoAdapter(List<Persona> lstApodo, Context context) {

        this.lstApodos = lstApodo;
        this.context = context;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //Este metodo hace la relacion con el layout del item
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_apodo, parent, false);


        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        //Este metodo itera de acuerdo a lo que indicas en el metodo getItemCount
        Persona persona = lstApodos.get(position);
        holder.tviApodo.setText(persona.nombre);
        holder.iviImagen.setImageDrawable(context.getResources().getDrawable(persona.idImagen));


    }

    @Override
    public int getItemCount() {
        //Aqui indicas cuantas filas tendra tu RecyclerView
        return lstApodos.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {

        TextView tviApodo;
        ImageView iviImagen;

        public ViewHolder(View v) {
            super(v);
            tviApodo = v.findViewById(R.id.tviApodo);
            iviImagen = v.findViewById(R.id.iviImagen);


        }
    }

}
