package com.pe.listado;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    RecyclerView rviListado;
    ApodoAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        rviListado = findViewById(R.id.rviListado);

        //Mostrar la informacion de forma lineal
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);

        //Creando Datos para pasar al adapter

        List<Persona> lstPersonas = new ArrayList<>();
        lstPersonas.add(new Persona("RIccardo", "Mija", R.drawable.luisfgn));
        lstPersonas.add(new Persona("Miguel", "Balcazar", R.drawable.luisfgn));
        lstPersonas.add(new Persona("Oscar", "Ortiz", R.drawable.pbznl));
        lstPersonas.add(new Persona("Pedro", "Bazan", R.drawable.pbznl));

        //Instanciamos el Adapter
        adapter = new ApodoAdapter(lstPersonas, this);


        //Llenar informacion listado
        rviListado.setHasFixedSize(true);
        rviListado.setLayoutManager(layoutManager);
        rviListado.setAdapter(adapter);


    }
}
